from django.core.mail import send_mail

from .models import EMail, Veranstaltung

REGISTRATION_BODY="""
Hallo %s,

vielen Dank für deine Anmeldung zur %s. Um deine Anmeldung abzuschließen klicke auf den folgenden Link. Über den Link kannst du auch später noch deine Angaben überprüfen und ändern.

https://gaf.fs.lmu.de/ophase/anmeldung/%s

Viele Grüße,
deine Fachschaft GAF
"""

VERIFICATION_BODY="""
Hallo %s,

um die Änderung deiner E-Mailadresse für die %s zu bestätigen klicke auf den folgenden Link.

https://gaf.fs.lmu.de/ophase/anmeldung/%s

Viele Grüße,
deine Fachschaft GAF
"""

def sendMail(body, email):
    veranstaltung = Veranstaltung.get_current()

    send_mail(subject="%s Anmeldung"%veranstaltung,
        message=body%(email.ersti.vorname,veranstaltung,email.get_link()),
        from_email='O-Phase <ophase@fs.lmu.de>',
        recipient_list=[email.email]
    )

def sendRegistrationMail(email):
    sendMail(REGISTRATION_BODY, email)

def sendVerificationMail(email):
    sendMail(VERIFICATION_BODY, email)
