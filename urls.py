from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.AnmeldeView.as_view(), name='anmeldung'),
    url(r'^([a-zA-Z0-9_-]{22})/$', views.ErstiView.as_view(), name='ersti'),
    url(r'^([a-zA-Z0-9_-]{22})/edit$', views.ErstiEditView.as_view(),
        name='ersti_edit'),
]
