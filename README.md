# O-Phase Anmeldeskript

## Ziele
- [ ] Anmeldung
    - [ ] Anmeldezeiträume für Veranstaltungen
    - [x] Erstis können sich anmelden (obviously)
    - [ ] Nach der Anmeldung können Daten bearbeitet werden
        - [ ] E-Mail
	- [x] Sonstige Daten
    - [x] Mailadressen müssen bestätigt werden
    - [ ] Nach anmeldung Link neu zusenden lassen
- [ ] Kommentare
    - [x] Kommentieren
    - [ ] Wer hat kommentiert (Ersti / Admin)
- [ ] Raumzuteilung
    - [ ] Möglichkeit jedem Ersti einen Raum zur Begrüßung zuzuordnen
    - [ ] Nachträgliches Zuordnen von Räumen
    - [ ] Mails versenden mit Rauminformation
- [ ] Auswertung
    - [ ] Einfaches Filtern der Erstis für Tutorzuteilung
    - [ ] Dumpen der Mailadressen um sie auf die Listen hinzuzufügen
- [ ] Anonymisieren von Daten nach einiger Zeit

## Datenbankschema
Es gibt Veranstaltungen, Studiengänge und Erstis. Zu Erstis können
Kommentare und Mailadressen zugeordnet werden. Ein Ersti kann temporär
mehrere Mailadressen haben wenn er seine Mailadresse ändern will, die neue
aber noch nicht bestätigt ist.

## Links
Um auf seine Daten zuzugreifen erhält jeder Student einen eigenen Link der
aus der uuid der Mailadresse generiert wird. Jeder der den Link kennt kann
auf die Daten zugreifen und Änderungen durchführen. Wird die Mailadresse
geändert gibt es einen neuen Link, der alte bleibt dabei gültig.
