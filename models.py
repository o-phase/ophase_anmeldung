from django.db import models
from django.utils import timezone

import base64
import uuid

class Veranstaltung(models.Model):
    name = models.CharField(max_length = 50)
    anmeldung_beginn = models.DateTimeField()
    anmeldung_ende = models.DateTimeField()

    def __str__(self):
        return self.name

    def get_current():
        return Veranstaltung.objects.filter(anmeldung_beginn__lte=timezone.now(),
            anmeldung_ende__gte=timezone.now())[0]

class Studienfach(models.Model):
    ABSCHLUSS= (
        ('BA', 'Bachelor'),
        ('MA', 'Master'),
        ('Lv', 'Lehramt vertieft'),
        ('Ln', 'Lehramt nicht vertieft'),
    )
    FACHGRUPPE=(
        ('Ph', 'Physik'),
        ('In', 'Informatik'),
        ('Mi', 'Medieninformatik'),
        ('Ma', 'Mathematik'),
    )
    abschluss = models.CharField(max_length=2, choices=ABSCHLUSS)
    fachgruppe = models.CharField(max_length=2, choices=FACHGRUPPE)
    name = models.CharField(max_length=100)
    hauptfach = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Ersti(models.Model):
    ANREDE=(
        ('Hr', 'Herr'),
        ('Fr', 'Frau'),
        ('So', 'Sonstig'),
    )
    veranstaltung = models.ForeignKey(Veranstaltung)
    anrede = models.CharField(max_length=2, choices=ANREDE, default='So')
    vorname = models.CharField(max_length = 50)
    nachname = models.CharField(max_length = 50)
    studienfach = models.ForeignKey(Studienfach)
    infoverteiler = models.BooleanField(default = False)
    newsverteiler = models.BooleanField(default = False)
    datum = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.vorname + ' ' + self.nachname

class EMail(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4,
        editable=False)
    email = models.EmailField(null=True)
    verifiziert = models.BooleanField(default=False)
    ersti = models.ForeignKey(Ersti)

    def __str__(self):
        if self.email:
            return self.email
        else:
            return 'E-Mail entfernt'

    def get_link(self):
        return base64.urlsafe_b64encode(self.uid.bytes)[0:22].decode('utf-8')

    def from_link(link):
        return EMail.objects.get(uid=uuid.UUID(bytes=base64.urlsafe_b64decode(link+'==')))

class Kommentar(models.Model):
    ersti = models.ForeignKey(Ersti)
    text = models.TextField()
    datum = models.DateTimeField(auto_now_add=True)
